(function ($) {
  $(document).ready(function () {
    $('.header-menu.big-screen li.has-child').hover(
      function () {
        $(this).addClass('selected');
      },
      function () {
        $(this).removeClass('selected');
      });

    //hack right column height
    var container_height = $('.container').height();
    $('.right-column').css('height', container_height + 'px');

    $('.language-switcher li').on('click', function () {
      $(this).addClass('selected');
      $('.language-switcher li').not($(this)).removeClass('selected');
    });

    $('.block-head').on('click', function () {
      var parent = $(this).closest('aside');
      var this_block = $(this).closest('.block');
      var target = this_block.find('.block-text');
      var other_targets = parent.find('.block-text').not(target);

      if (target.hasClass('closed')) {
        target.stop().slideDown(850).removeClass('closed').addClass('opened');
      } else {
        target.stop().slideUp(850).removeClass('opened').addClass('closed');
      }

      if (other_targets.hasClass('opened')) {
        other_targets.stop().slideUp(850).removeClass('opened').addClass('closed')
      }
    });
  });
})(jQuery);

